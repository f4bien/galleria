<%@ include file="../../commons/declarations.jspf" %>

<jcr:nodeProperty node="${currentNode}" name="folder" var="folder"/>

<c:set var="isEmpty" value="true"/>

<c:set var="galleriaID" value="galleria-${currentNode.UUID}"/>

<c:if test="${not empty folder}">
  <c:set var="folder" value="${folder.node}"/>
  <c:set var="fileList" value="${jcr:getChildrenOfType(folder,'jnt:file')}"/>
</c:if>

<c:if test="${not isEditMode}">
  <template:addResources type="inlinecss">  
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/galleria.classic.min.css"/>
    <style type="text/css"> 
      #${galleriaID}{
        height: 500px;
        max-height: 500px;
      }
      #${galleriaID} .galleria-theme-classic .galleria-loader {
        background-image: url(//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/classic-loader.gif);
      }
      #${galleriaID} .galleria-theme-classic .galleria-thumb-nav-left,
      #${galleriaID} .galleria-theme-classic .galleria-thumb-nav-right,
      #${galleriaID} .galleria-theme-classic .galleria-info-link,
      #${galleriaID} .galleria-theme-classic .galleria-info-close,
      #${galleriaID} .galleria-theme-classic .galleria-image-nav-left,
      #${galleriaID} .galleria-theme-classic .galleria-image-nav-right {
        background-image: url(//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/classic-map.png);
      }
    </style>
  </template:addResources>
</c:if>

<div id="${galleriaID}" class="galleria">
  <c:forEach items="${fileList}" var="child" varStatus="status">
    <c:choose>
      <c:when test="${isEditMode}">
        <c:if test="${fn:startsWith(child.fileContent.contentType,'image/')}">
          <div class="thumbnail">
            <template:module node="${child}" view="thumbnail"/>
            <div class="caption">
              ${fn:escapeXml(child.displayableName)}
            </div>
          </div>
          <c:set var="isEmpty" value="false"/>
        </c:if>
      </c:when>
      <c:otherwise>
        <c:choose>
          <c:when test="${fn:startsWith(child.fileContent.contentType,'image/')}">
            <c:url var="imgUrl" value="${child.url}"/>
            <c:set var="imgTitle" value="${child.displayableName}"/>
            <c:set var="imgDesc" value="${child.properties['jcr:description'].string}"/>
            <a href="${imgUrl}">
              <template:module node="${child}" view="thumbnail">
                <template:param name="attrData">
                  data-big="${imgUrl}"
                  data-title="${imgTitle}"
                  data-description="${imgDesc}"
                </template:param>
              </template:module>
            </a>
          </c:when>
          <c:otherwise>
            <template:module node="${child}"/>
          </c:otherwise>
        </c:choose>
        <c:set var="isEmpty" value="false"/>
      </c:otherwise>
    </c:choose>
  </c:forEach>
  
  <c:if test="${isEmpty}">
    <p><fmt:message key="galleria.empty"/></p>
  </c:if>
  
  <c:if test="${renderContext.editMode}">
    <template:module path="*"/>
  </c:if>
</div>

<c:if test="${not isEditMode}">
  <script src="//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/galleria.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/galleria.classic.min.js"></script>
  <script src="//cdn.rawgit.com/aiaio/galleria-facebook/master/facebook/galleria.facebook.js"></script>
  <script>
    var app = (function(app, $) {
      $(function () {
        (function (Galleria) {
          Galleria.ready(function() {
            this.attachKeyboard({
              left: this.prev,
              right: this.next
            });
          });
          Galleria.run('#${galleriaID}', {
            height: 0.5625,
            lightbox: true
          });
          app.galleria = Galleria.get(0);
        }(Galleria));
      });
      return app;
    }(app || {}, jQuery));
  </script>
</c:if>