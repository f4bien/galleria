<%@ include file="../../commons/declarations.jspf" %>

<c:set var="galleriaID" value="galleria-${currentNode.UUID}"/>

<c:if test="${not isEditMode}">
  <template:addResources type="inlinecss">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/galleria.classic.min.css"/>
    <style type="text/css"> 
      #${galleriaID}{
        height: 520px;
        max-height: 550px;
      }
      #${galleriaID} .galleria-theme-classic .galleria-loader {
        background-image: url(//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/classic-loader.gif);
      }
      #${galleriaID} .galleria-theme-classic .galleria-thumb-nav-left,
      #${galleriaID} .galleria-theme-classic .galleria-thumb-nav-right,
      #${galleriaID} .galleria-theme-classic .galleria-info-link,
      #${galleriaID} .galleria-theme-classic .galleria-info-close,
      #${galleriaID} .galleria-theme-classic .galleria-image-nav-left,
      #${galleriaID} .galleria-theme-classic .galleria-image-nav-right {
        background-image: url(//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/classic-map.png);
      }
    </style>
  </template:addResources>
  <script src="//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/galleria.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/galleria/1.4.2/themes/classic/galleria.classic.min.js"></script>
  <script src="//cdn.rawgit.com/aiaio/galleria-facebook/master/facebook/galleria.facebook.js"></script>
  <script>
    var app = (function(app, $) {
      $(function () {
        (function (Galleria) {
          Galleria.ready(function() {
            this.attachKeyboard({
              left: this.prev,
              right: this.next
            });
          });
          Galleria.run('#${galleriaID}', {
            height: 0.5625,
            lightbox: true,
            facebook: 'album:${currentNode.properties.albumId.string}',
            facebookOptions: {
              max: 30,
              facebook_access_token: '${currentNode.properties.facebookAccessToken.string}'
            }
          });
          app.galleria = Galleria.get(0);
        }(Galleria));
      });
      return app;
    }(app || {}, jQuery));
  </script>
</c:if>