<%@ include file="../../commons/declarations.jspf" %>

<c:set var="galleriaID" value="galleria-${currentNode.UUID}"/>

<div id="${galleriaID}" class="galleria"></div>

<template:include view="hidden.resources" />