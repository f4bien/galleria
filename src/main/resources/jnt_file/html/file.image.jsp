<%@ include file="../../commons/declarations.jspf" %>

<jcr:nodeProperty node="${currentNode}" name="jcr:title" var="title"/>
<jcr:nodeProperty node="${currentNode}" name="jcr:description" var="description" />
<jcr:nodeProperty node="${currentNode}" name="author" var="author" />
<jcr:nodeProperty node="${currentNode}" name="legend" var="legend" />

<c:if test="${not empty thumbnail.properties['j:height']}">
  <c:set var="height">height="${thumbnail.properties['j:height'].string}"</c:set>
</c:if>

<c:if test="${not empty thumbnail.properties['j:width']}">
  <c:set var="width">width="${thumbnail.properties['j:width'].string}"</c:set>
</c:if>

<img src="${currentNode.url}?t=thumbnail2"
     srcset="${currentNode.url}?t=thumbnail2 1x, ${currentNode.url} 2x" 
     ${not empty attrClassValue ? ' class="${attrClassValue}"':''}
     ${not empty attrItempropValue ? ' itemprop="${attrItempropValue}"':''}
     ${not empty title ? ' title="${fn:escapeXml(title.string)}"':''}
     ${not empty description ? ' alt="${fn:escapeXml(description.string)}"':''}
     ${not empty height ? ' height="${height}"':''}
     ${not empty width ? ' width="${width}"':''}
     ${not empty attrData ? ' ${attrData}':''}
/>